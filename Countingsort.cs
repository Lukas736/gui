﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace Sorting_alghoritms
{
    class CountingSort
    {
        private string path;

        public CountingSort(string path)
        {
            this.path = path;
        }

        public int[] Sort()
        {
            List<int> pole;
            pole = FileSlave.Read(path);

            int max = pole.Max();
            int min = pole.Min();
            int[] pocetC = new int[max - min + 1];
            int[] serazenePole = new int[pole.Count];

            //Zjištění četností hodnot klíčů ve vstupním poli
                for (int i = 0; i < pole.Count; i++)
                {
                    pocetC[pole[i] - min]++;
                }

                //Přepočítání pole četnosti C
                pocetC[0]--;
                for (int i = 1; i < pocetC.Length; i++)
                {
                    pocetC[i] = pocetC[i - 1] + pocetC[i];
                }

                //Umístění klíčů do výstupního pole 'serazenePole' na správné místo

                for (int i = pole.Count - 1; i >= 0; i--)
                {

                    serazenePole[pocetC[pole[i] - min]] = pole[i];
                    pocetC[pole[i] - min]--;
                }

            return serazenePole;
        }


    }
}
