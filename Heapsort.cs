﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Diagnostics;
using System.Threading.Tasks;
using System.Windows;

namespace Sorting_alghoritms
{
    class HeapSort
    {
        private string path;
        public HeapSort(string path)
        {
            this.path = path;
        }


        public List<int> Sort()
        {
            List<int> pole = FileSlave.Read(path);
            Heapify(pole); //Vytvoření haldy
            int index = pole.Count - 1; //Poslední prvek
            int pom;

                while (index > 0)
                {
                    pom = pole[0]; //Prohození posledního prvku s posledním
                    pole[0] = pole[index];
                    pole[index] = pom;
                    index -= 1; //Nový poslední prvek
                    HeapDown(pole, index);
                }

            return pole;
        }


        private void HeapUp(List<int> pole, int i)
        {

            int potomek = i; //uložení potomka
            int rodic;
            while (potomek != 0)
            {
                rodic = (potomek - 1) / 2; //rodič
                if (pole[rodic] < pole[potomek])
                {
                    int pom = pole[potomek];  //prohození rodiče s potomkem
                    pole[potomek] = pole[rodic];
                    pole[rodic] = pom;
                    potomek = rodic; //nový potomek
                }
                else
                {
                    return;
                }

            }
        }

        private List<int> HeapDown(List<int> pole, int posledni)
        {
            int rodic = 0;
            int potomek;
            int pom;
            while (rodic * 2 + 1 <= posledni)
            {
                potomek = rodic * 2 + 1;
                //Je vybrán menší potomek
                if ((potomek < posledni) && (pole[potomek] < pole[potomek + 1]))
                {
                    potomek++; //Vybereme toho většího
                }
                if (pole[rodic] < pole[potomek])
                {
                    pom = pole[rodic]; //Prohození rodiče s potomkem
                    pole[rodic] = pole[potomek];
                    pole[potomek] = pom;
                    rodic = potomek; //Nový rodič
                }
                else
                {
                    break;
                }
            }
            return pole;
        }

        public void Heapify(List<int> pole)
        {
            for (int i = 1; i < pole.Count; i++)
            {
                HeapUp(pole, i);
            }
        }

    




    }
}
