﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Windows;
using System.Diagnostics;

namespace Sorting_alghoritms
{
    class BubbleSort
    {
        private string path;

        public BubbleSort(string path)
        {
            this.path = path;
        }


        private List<int> Sort()
        {
            List<int> pole;
            pole = FileSlave.Read(path);

            int i = 0;
            int prohozeni = 1;

                while ((i < pole.Count - 1) && (prohozeni > 0))
                {
                    prohozeni = 0;
                    for (int j = 0; j < pole.Count - 1 - i; j++)
                    {
                        if (pole[j] > pole[j + 1])
                        {
                            int pom = pole[j];
                            pole[j] = pole[j + 1];
                            pole[j + 1] = pom;
                            prohozeni++;

                        }

                    }
                    i++;
                }

            return pole;

        }




    }
}
