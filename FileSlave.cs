﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Sorting_alghoritms
{
    static class FileSlave
    {
        public static List<int> Read(string path)
        {
            path += "\\numbers.txt";
            List<int> pole = new List<int>();
            TextReader f_in = File.OpenText(path);
            string s1;
            while ((s1 = f_in.ReadLine()) != null)
            {
                pole.Add(Convert.ToInt32(s1));
            }
            return pole;
        }


        public static void Write(string path, List<int> pole = null , int[] Q = null)
        {
            if (pole != null)
            {
                using (TextWriter f_out = File.CreateText(path))
                    foreach (int b in pole)
                        f_out.WriteLine(b);
            }
            else if (Q != null)
            {
                using (TextWriter f_out = File.CreateText(path))
                    foreach (int b in Q)
                        f_out.WriteLine(b);
            }
            else
            {
                throw new Exception("Obě pole nemůžou být null!");
            }
            
          
            
        }




    }
}
